# USAFA CS364 Bookswap Development Steps

## Initialization
1. make directory
2. npm init
3. modify package.json
4. create .gitignore
5. create JSON data files
6. add import commands to initialize.bat for each seed:

    mongoimport --db <dbname> --collection <collection-name> --type json --file <file-name> --jsonArray --drop

## Getting Started
1. create app.js
2. configure express app
3. setup rotues
4. npm install
