angular.module('BookSwap').factory('Condition', function ConditionFactory($resource) {
  return $resource('/conditions/:conditionName');
});
