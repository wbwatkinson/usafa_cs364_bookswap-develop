angular.module('BookSwap').factory('Item', function ItemFactory($resource) {
  return $resource('/items/:id');
});
