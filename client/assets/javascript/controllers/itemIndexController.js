angular.module('BookSwap').controller('ItemsIndexController', function(Item, Condition, GoogleAPI, $scope) {
  $scope.items = [];
  var items = Item.query(function() {
    items.forEach(function(item) {
      if (!item.sold) {
        var itemDetails = GoogleAPI.get({
          isbn: item.isbn
        }, function() {
          item.title = itemDetails.items[0].volumeInfo.title;
          item.description = itemDetails.items[0].volumeInfo.description;
          item.authors = itemDetails.items[0].volumeInfo.authors.join(', ');
          item.publisher = itemDetails.items[0].volumeInfo.publisher;
          item.publishedDate = itemDetails.items[0].volumeInfo.publishedDate;
          item.imageUrl = itemDetails.items[0].volumeInfo.imageLinks.thumbnail;
          item.pageCount = itemDetails.items[0].volumeInfo.pageCount;
        });
        $scope.items.push(item);
      }
    });
  });

  $scope.conditions = Condition.query();

});
