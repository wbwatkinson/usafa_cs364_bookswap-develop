angular.module('BookSwap').controller('ItemsEditController', function (Item, Condition, GoogleAPI, $scope, $routeParams){
  $scope.item = Item.get({id: $routeParams.id}, function() {

    var itemDetails = GoogleAPI.get({isbn: $scope.item.isbn}, function() {
      $scope.item.title = itemDetails.items[0].volumeInfo.title;
      $scope.item.description = itemDetails.items[0].volumeInfo.description;
      $scope.item.authors = itemDetails.items[0].volumeInfo.authors.join(', ');
      $scope.item.publisher = itemDetails.items[0].volumeInfo.publisher;
      $scope.item.publishedDate = itemDetails.items[0].volumeInfo.publishedDate;
      $scope.item.imageUrl = itemDetails.items[0].volumeInfo.imageLinks.thumbnail;
      $scope.item.pageCount = itemDetails.items[0].volumeInfo.pageCount;
    });
  });

  $scope.conditions = Condition.query();

});
