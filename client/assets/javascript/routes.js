angular.module('BookSwap').config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      redirectTo: '/items'
    })

    .when('/items', {
      templateUrl: "assets/templates/items/index.html",
      controller: "ItemsIndexController"
    })

    .when('/items/new', {
      templateUrl: "assets/templates/items/new.html",
      controller: "ItemsCreateController"
    })

    .when('/items/:id/edit', {
      templateUrl: "assets/templates/items/edit.html",
      controller: "ItemsEditController"
    })

    .when('/items/:id', {
      templateUrl:"assets/templates/items/show.html",
      controller: "ItemsShowController"
    })

});
