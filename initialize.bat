mongoimport --db bookswap --collection courses --type json --file server\seed-courses.json --jsonArray --drop

mongoimport --db bookswap --collection users --type json --file server\seed-users.json --jsonArray --drop

mongoimport --db bookswap --collection items --type json --file server\seed-items.json --jsonArray --drop

mongoimport --db bookswap --collection conditions --type json --file server\seed-conditions.json --jsonArray --drop
