var Condition = require('../models/condition');

module.exports = function(app) {
  app
    .get('/conditions', function(req,res) {
      Condition.all(function (err,docs) {
        docs.forEach(function(condition) {
          delete condition.password;
          delete condition.role;
        });
        res.json(docs);
      });
    })

    .get('/conditions/:conditionid', function(req,res) {
      var conditionid = req.params.conditionid;

      Condition.get(conditionid, function(err,doc) {
        delete doc.password;
        delete doc.role;

        res.json(doc);
      });
    })
};
