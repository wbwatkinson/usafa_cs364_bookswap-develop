var Item = require('../models/item');

var createItem = function (req,res,id,item) {
  item.id = id;
  item.name = req.body.name;
  item.title = req.body.title;
  item.description = req.body.description;
};

module.exports = function(app) {
  app
    .get('/items', function(req,res) {
      Item.all(function (err,docs) {
        res.json(docs);
      });
    })

    .get('/items/isbn/:isbn', function(req,res) {
      console.log('matched');
      var isbn = req.params.isbn;
      console.log('isbn: ' + isbn);
      Item.getByIsbn(isbn, function(err,docs) {
        res.json(docs);
      });
    })

    .get('/items/:id', function(req,res) {
      var _id = req.params.id;
      Item.get(_id, function(err,doc) {
        res.json(doc);
      });
    })

    .post('/items/:id', function(req,res) {
      var item = {};
      createItem(req,res,req.params.id,item);

      Item.update(item, function(err,doc) {
        res.json(item);
      });
    })

};
