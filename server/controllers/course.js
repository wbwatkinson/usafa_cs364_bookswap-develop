var Course = require('../models/course');

var createCourse = function (req,res,courseid,course) {
  course.courseid = courseid;
  course.name = req.body.name;
  course.title = req.body.title;
  course.description = req.body.description;
};

module.exports = function(app) {
  app
    .get('/courses', function(req,res) {
      Course.all(function (err,docs) {
        docs.forEach(function(course) {
          delete course.password;
          delete course.role;
        });
        res.json(docs);
      });
    })

    .get('/courses/:courseid', function(req,res) {
      var courseid = req.params.courseid;

      Course.get(courseid, function(err,doc) {

        res.json(doc);
      });
    })

    .get('/courses/:courseid/books', function(req,res) {
      var courseid = req.params.courseid;

      Course.get(courseid, function(err,doc) {

        res.json(doc.books);
      });
    })

    .get('/courses/:courseid/isbns', function(req,res) {
      var courseid = req.params.courseid;

      Course.get(courseid, function(err,doc) {
        var isbns = [];
        console.log(doc.books[0].versions[0]);
        for (var i=0; i<doc.books.length; i++) {
          for (var j=0; j<doc.books[i].versions.length; j++) {
            isbns.push(doc.books[i].versions[j].isbn);
          }
        }
        res.json(isbns);
      });
    })

    .put('/courses/:courseid', function(req,res) {
      var course = {};
      createCourse(req,res,req.params.courseid,course);


      Course.update(course, function(err,doc) {
        res.json(course);
      });
    })

};
