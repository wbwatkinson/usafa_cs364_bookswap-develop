var User = require('../models/user');

var createUser = function (req,res,userid,user) {
  user.userid = userid;
  user.firstName = req.body.firstName;
  user.lastName = req.body.lastName;
  user.data = {};
  user.data.squadron = req.body.squadron;
  user.data.year = req.body.year;
  user.data.cell = req.body.cell;
  user.data.email = req.body.email;
};

module.exports = function(app) {
  app
    .get('/users', function(req,res) {
      User.all(function (err,docs) {
        docs.forEach(function(user) {
          delete user.password;
          delete user.role;
        });
        res.json(docs);
      });
    })

    .get('/users/:userid', function(req,res) {
      var userid = req.params.userid;

      User.get(userid, function(err,doc) {
        delete doc.password;
        delete doc.role;

        res.json(doc);
      });
    })

    .put('/users/:userid', function(req,res) {
      var user = {};
      createUser(req,res,req.params.userid,user);


      User.update(user, function(err,doc) {
        res.json(user);
      });
    })

};
