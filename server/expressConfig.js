var bodyParser = require('body-parser'),
    logger = require('morgan');

module.exports = function(app,express) {
  app.use('/', express.static("client/"));

  // set the view directory, enabling use of the .render method inside routes
  app.set('views', __dirname + '/../client/views');

  // log requests
  app.use(logger('dev'));

  // parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: false }));

  // parse applicatio/json
  app.use(bodyParser.json());
};
