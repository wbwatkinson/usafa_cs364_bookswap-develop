var mongo = require('mongodb');
var connection = mongo.MongoClient;

exports.connect = function() {
  if(mongo.DB) {return mongo.DB}

  connection.connect('mongodb://localhost:27017/bookswap', function (err, db) {
    if (err) {
      console.log('Check mongod server');
      process.exit();
    } else {
      mongo.DB = db;
      console.log('Mongo connection established');
      return mongo.DB;
    }
  });
};
