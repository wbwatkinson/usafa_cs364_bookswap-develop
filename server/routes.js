module.exports = function(app) {
  app.get('/', function(req, res) {
    res.sendFile('index.html', {root: app.settings.views});
  });

  // Load routes
  require('./controllers/user')(app); // user routes
  require('./controllers/course')(app); // course routes
  require('./controllers/item')(app); // item routes
  require('./controllers/condition')(app); // conditon routes
};
