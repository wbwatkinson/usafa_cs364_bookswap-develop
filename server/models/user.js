var mongo = require('mongodb');
var mongoUtil = require('../services/mongoUtil');

mongoUtil.connect();

module.exports = {
  get: function(userid, cb) {
    var collection = mongo.DB.collection('users');

    if (typeof userid === 'string') {
      collection.findOne({userid: userid}, function(err,doc) {
        cb(err,doc);
      });
    } else {
      cb(true,null);
    }
  },

  all: function(cb) {
    var collection = mongo.DB.collection('users');

    collection.find().toArray(function (err,docs) {
      cb(err,docs);
    });
  },

  update: function(user, cb) {
    var collection = mongo.DB.collection('users');

    collection.replaceOne({userid: user.userid}, user, function (err,result) {
      cb(err,result);
    });
  },

  create: function(user,cb) {
    var collection = mongo.DB.collection('users');

    collection.insertOne(user, function(err,result) {
      cb(err,result);
    });
  },

  delete: function(userid,cb) {
    var collection = mongo.DB.collection('users');

    collection.deleteOne({userid:userid}, function(err,results) {
      cb(err,results);
    });
  }
};
