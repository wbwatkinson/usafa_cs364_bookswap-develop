var mongo = require('mongodb');
var mongoUtil = require('../services/mongoUtil');

mongoUtil.connect();

module.exports = {
  get: function(courseid, cb) {
    var collection = mongo.DB.collection('courses');

    if (typeof courseid === 'string') {
      collection.findOne({courseid: courseid}, function(err,doc) {
        cb(err,doc);
      });
    } else {
      cb(true,null);
    }
  },

  all: function(cb) {
    var collection = mongo.DB.collection('courses');

    collection.find().toArray(function (err,docs) {
      cb(err,docs);
    });
  },

  update: function(course, cb) {
    var collection = mongo.DB.collection('courses');

    collection.replaceOne({courseid: course.courseid}, course, function (err,result) {
      cb(err,result);
    });
  },

  create: function(course,cb) {
    var collection = mongo.DB.collection('courses');

    collection.insertOne(course, function(err,result) {
      cb(err,result);
    });
  },

  delete: function(courseid) {
    var collection = mongo.DB.collection('courses');

    collection.deleteOne({courseid:courseid}, function(err,results) {
      cb(err,results);
    });
  }
};
