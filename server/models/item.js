var mongo = require('mongodb');
var mongoUtil = require('../services/mongoUtil');
var ObjectId = mongo.ObjectID;

mongoUtil.connect();

module.exports = {
  get: function(id, cb) {
    var collection = mongo.DB.collection('items');

    if (typeof id === 'string') {
      collection.findOne({_id: new ObjectId(id)}, function(err,doc) {
        cb(err,doc);
      });
    } else {
      cb(true,null);
    }
  },

  getByIsbn: function(isbn,cb) {
    var collection = mongo.DB.collection('items');

    if (typeof isbn === 'string') {
      collection.find({isbn: isbn}).toArray(function (err,doc) {
        console.log(err);
        console.log(doc);
        cb(err,doc);
      });
    } else {
      cb(true,null);
    }
  },

  all: function(cb) {
    var collection = mongo.DB.collection('items');

    collection.find().toArray(function (err,docs) {
      cb(err,docs);
    });
  },

  update: function(item, cb) {
    var collection = mongo.DB.collection('items');

    collection.replaceOne({_id: item.id}, item, function (err,result) {
      cb(err,result);
    });
  },

  create: function(item,cb) {
    var collection = mongo.DB.collection('items');

    collection.insertOne(item, function(err,result) {
      cb(err,result);
    });
  },

  delete: function(id) {
    var collection = mongo.DB.collection('items');

    collection.deleteOne({_id:id}, function(err,results) {
      cb(err,results);
    });
  }
};
