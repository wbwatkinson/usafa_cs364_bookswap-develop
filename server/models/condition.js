var mongo = require('mongodb');
var mongoUtil = require('../services/mongoUtil');

mongoUtil.connect();

module.exports = {
  get: function(condition, cb) {
    var collection = mongo.DB.collection('conditions');

    if (typeof condition === 'string') {
      collection.findOne({condition: condition}, function(err,doc) {
        cb(err,doc);
      });
    } else {
      cb(true,null);
    }
  },

  all: function(cb) {
    var collection = mongo.DB.collection('conditions');

    collection.find().toArray(function (err,docs) {
      cb(err,docs);
    });
  }

};
