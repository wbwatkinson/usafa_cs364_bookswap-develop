var express = require('express');

var app = express();

// Load Express Configuration
require('./expressConfig')(app,express);

// Load Routes
require("./routes")(app);

// Start the server
var port = process.argv[2] || 8080;
app.listen(port, function() {
  console.log('Listening on port ' + port + '...');
});
